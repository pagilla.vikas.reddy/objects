const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }

const { keys , values , mapObj , pairs , invert , defaults } = require('./Objects')

//keys
console.log('result of keys : ')
console.log(keys(testObject))

//values
console.log('result of values :')
console.log(values(testObject))

//mapObj
console.log('result of mapObject :')
console.log(mapObj(testObject , (val, key)=> val+2))

//pairs
console.log('result of pairs :')
console.log(pairs(testObject))

//invert
console.log('result of invert :')
console.log(invert(testObject))

//defaults
console.log('result of defaults :')
console.log(defaults(testObject , {name: 'Bruce Wayne', age: '22', location: 'Hyd'}))
