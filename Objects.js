//keys
function keys(testObject){
    let l1=[]
    for (let i in testObject)
        l1.push(i)
    return l1
}
//values
function values(testObject) {
    let l1=[]
    for (let i in testObject)
        l1.push(testObject[i])
    return l1
}
//mapObjects
function mapObj(obj, cb){
    let new_obj = {}
    for ( let i in obj ){
        new_obj = cb(obj[i], obj)
        obj[i] = new_obj
    }
    return obj
}
//pairs
function pairs(obj){
    let l1 = []
    for ( let i in obj)
        l1.push( i , obj[i])
    return l1
}
//invert
function invert(obj){
    let obj1 = {}
    for ( let key in obj){
        obj1[obj[key]] = key
    }
    return obj1
}
function defaults(obj, defaultProps){
    for ( let key in obj ){
        if ( obj[key] != defaultProps[key])
            obj[key] = defaultProps[key]  
    }
    return obj
    
}

module.exports = { keys , values , mapObj , pairs , invert , defaults }